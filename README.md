
# Interpreter and Simulator for Process Algebra with Hooks models

Author: Andrea Degasperi, University of Glasgow 2011

Version: 2011-05-20

### Table of Content
[TOC]

### Publications on Process Algebra with Hooks

For details about the algebra and how to use it see the
following publications:

- A. Degasperi and M. Calder. **A Process Algebra Framework for
  Multi-Scale Modelling of Biological Systems.** *Theoretical
  Computer Science*, Volume 488, pages 15-45, Elsevier, 2013.
- A. Degasperi and M. Calder. **Multi-Scale Modelling of
  Biological Systems in Process Algebra with Multi-Way
  Synchronisation.** *Proceedings of the 9th International
  Conference on Computational Methods in Systems Biology (CMSB
  2011)*, pages 195-208, ACM, 2011.



### Installation/Compilation

Linux, ```ocaml > 3.08``` only. Make sure you have ```ocamlc```, ```ocamllex```
and ocamlyacc installed.

To compile the program:

- enter the directory ```source```
- execute ```./compile.sh```

This will create the binary file ```interpret``` in the main
directory.


### Running the Interpreter

Write your PAH model in a file such as ```model.pah```. Run the
program with the following command:

```
./interpret model.pah simulate 10
```

This will run a simulation of the model up to 10 time units.
In particular this command:

1. reads the model in model.pah;
2. computes the one step moves (transitions) from the initial
state;
3. determines which one step moves are open moves and which
are closed moves;
4. assigns a rate to closed moves using functional rates;
5. uses the rated moves to sample a time delay and determine
the next current state;
6. repeats from point 2. until the given time (here 10) is
reached.



### PAH Syntax

The file ```model.pah``` should contain:

- the definition of a model process, which is the initial
state of the system;
- the definition of the agents used;
- optional constant used in the functional rate definitions;
- the functional rate definitions.

In details:


#### Model Process

For example:

```
model A(0) [t(0,1),t(1,0)] A(1);
```

where agent ```A(0)``` in *horizontal* synchronisation with agent ```A(1)```
on the action set ```t(0,1),t(1,0)```. To use the *vertical*
synchronisation operator, use the syntax ```P1 <actionSet> P2```.

For example:

```
model A(0) <x(0),y(0)> T(0);
```


#### Agent Definition

For example:

```
agent M(i),[M,i] = if i==0 then a.M(i+1)
                   else if i==1 then a[x].M(i+1) + b.M(i-1)
                   else b[y].M(i-1);
```

Where agent ```M(i)``` is defined, with associated variable ```M``` and
value ```i```.


#### Constants

For example:

```
const a = 1;
```

This defines a constant ```a```. You can use now ```a``` in the 
definition of functional rates or in agent definitions.


#### Functional Rates

For example:

```
rate t(i,j) = k*A(i)*h;
```

The rate of the transport reaction ```t``` from location ```i``` to ```j```
is given by the constant ```k```, times the levels of ```A(i)```, times
the step size ```h```.



