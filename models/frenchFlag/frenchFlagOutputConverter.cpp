#include <iostream>
#include <stdlib.h>
#include <sstream>
#include <string>
#include <math.h>
#include <fstream>

using namespace std;


int segments = 20;
int maxLevels = 20;
int * concentrations;
int * commitments;
int currentRun = 0;
const char * directory;


int main(int argc, const char* argv[])
{
  if (argc == 3) {
    currentRun = atoi(argv[2]);
    directory = argv[1];
  }
  //cin >> N;
  //cin >> M;
  concentrations = new int [segments];
  commitments = new int [segments];
  
  double currentTime;
  cin >> currentTime;
  int step = 0;
  //int frame = 0;
  int timeInterval = 0;
  string dummy = "";
  float dummy2 = 0;
  
  while (currentTime != -1 && step <= 200000) {
    //cout << "current time: " << currentTime << endl;
    //cout << "current step: " << step << endl;
    //int numberOfTissueRegions = 0;

    for (int i=0;i<segments;i++){
      cin >> dummy;
      //cout << "dummy: " << dummy << endl;
      cin >> dummy2;
      //cout << "dummy2: " << dummy2 << endl;
      cin >> dummy2;
      concentrations[i]=(int)dummy2;
      //cout << "concentration: " << concentrations[i] << endl;
      cin >> dummy;
      //cout << "dummy: " << dummy << endl;
      if (dummy=="TA"){
        commitments[i]=2;
        cin >> dummy2;
        //cout << "dummy: " << dummy << endl;
      }else if (dummy=="TB"){
        commitments[i]=1;
        cin >> dummy2;
        //cout << "dummy: " << dummy << endl;
      }else{
        commitments[i]=0;
        cin >> dummy2;
        //cout << "dummy: " << dummy << endl;
        cin >> dummy2;
        //cout << "dummy: " << dummy << endl;
        cin >> dummy2;
        //cout << "dummy: " << dummy << endl;
      }
    }
    

    if(currentTime >= timeInterval*0.3){

      //write in a file the concentration level for each region
      string st2;
      stringstream out2;
      out2 << maxLevels << "x" << 20 << "_" << directory << "_run" << currentRun << "/concentrations.txt";
      st2 = out2.str();    

      ofstream file2;
      const char * filename2 = st2.c_str();
      file2.open (filename2,ios::app);
      //file2 << numberOfTissueRegions << endl;
      file2 << timeInterval*0.3;
      for (int i=0;i<segments;i++) file2 << " " << concentrations[i];
      file2 << endl;
      file2.close();

      //-----------------------------------

      //write in a file the commitments for each region
      string st3;
      stringstream out3;
      out3 << maxLevels << "x" << 20 << "_" << directory << "_run" << currentRun << "/commitments.txt";
      st3 = out3.str();    

      ofstream file3;
      const char * filename3 = st3.c_str();
      file3.open (filename3,ios::app);
      //file3 << numberOfTissueRegions << endl;
      file3 << timeInterval*0.3;
      for (int i=0;i<segments;i++) file3 << " " << commitments[i];
      file3 << endl;
      file3.close();

      //-----------------------------------

      //picture of concentrations
      string st;
      stringstream out;
      //out << N << "x" << M << "_" << directory << "_run" << currentRun << "/tissueGrowth_t" << currentTime << "_s" << step << ".svg";
      out << maxLevels << "x" << 20 << "_" << directory << "_run" << currentRun << "/concentrations" << timeInterval << ".svg";
      st = out.str();    

      ofstream file;
      const char * filename = st.c_str();
      file.open (filename);
      file << "<?xml version=\"1.0\" standalone=\"yes\"?>\n";
      file << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n";
      file << "\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n";

      file << "<svg width=\"20cm\" height=\"5cm\" version=\"1.1\" viewBox=\"0 0 " << 400 << " " << 100 << "\" xmlns=\"http://www.w3.org/2000/svg\">\n";

      file << "<rect width=\"" << 400 << "\" height=\"" << 100 << "\" style=\"fill:rgb(255,255,255);stroke-width:2; stroke:rgb(0,0,0)\"/>\n";

      for (int j=0;j<segments;j++){
        //file << "<line y1=\"0\" x1=\"" << j*20 << "\" y2=\"" << 100 << "\" x2=\"" << j*20 << "\" style=\"stroke:rgb(0,0,0);stroke-width:1\"/>\n";
        file << "<rect y=\"" << 0 << "\" x=\"" << j*20 << "\" width=\"20\" height=\"100\" style=\"fill:rgb(" << (int)(255*(maxLevels-concentrations[j])/maxLevels) << "," << (int)(255*(maxLevels-concentrations[j])/maxLevels) << "," << (int)(255*(maxLevels-concentrations[j])/maxLevels) << ");stroke-width:0; stroke:rgb(0,0,0)\"/>\n";
      }

      file << "</svg>\n";
      file.close();

      //-----------------------------------

      //picture of commitments
      string st4;
      stringstream out4;
      //out << N << "x" << M << "_" << directory << "_run" << currentRun << "/tissueGrowth_t" << currentTime << "_s" << step << ".svg";
      out4 << maxLevels << "x" << 20 << "_" << directory << "_run" << currentRun << "/commitments" << timeInterval << ".svg";
      st4 = out4.str();    

      ofstream file4;
      const char * filename4 = st4.c_str();
      file4.open (filename4);
      file4 << "<?xml version=\"1.0\" standalone=\"yes\"?>\n";
      file4 << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"\n";
      file4 << "\"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\n";

      file4 << "<svg width=\"20cm\" height=\"5cm\" version=\"1.1\" viewBox=\"0 0 " << 400 << " " << 100 << "\" xmlns=\"http://www.w3.org/2000/svg\">\n";

      file4 << "<rect width=\"" << 400 << "\" height=\"" << 100 << "\" style=\"fill:rgb(255,255,255);stroke-width:2; stroke:rgb(0,0,0)\"/>\n";

      for (int j=0;j<segments;j++){
        //file << "<line y1=\"0\" x1=\"" << j*20 << "\" y2=\"" << 100 << "\" x2=\"" << j*20 << "\" style=\"stroke:rgb(0,0,0);stroke-width:1\"/>\n";
        file4 << "<rect y=\"" << 0 << "\" x=\"" << j*20 << "\" width=\"20\" height=\"100\" style=\"fill:rgb(" << (int)(255*(2-commitments[j])/2) << "," << (int)(255*(2-commitments[j])/2) << "," << (int)(255*(2-commitments[j])/2) << ");stroke-width:0; stroke:rgb(0,0,0)\"/>\n";
      }

      file4 << "</svg>\n";
      file4.close();


      timeInterval++;
    }
    cin >> currentTime;
    step++;
  }  
  
  delete []concentrations;
  delete []commitments;

  return 0;
}
