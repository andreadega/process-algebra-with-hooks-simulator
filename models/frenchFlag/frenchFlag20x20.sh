#! /bin/bash

X=1
while [ $X -le 100 ]
do
	echo $X
  mkdir 20x20_default_run${X}
	./interpret frenchflag.pah simulate 6 | ./outputConverter default ${X}
	X=$((X+1))
done

#X=72
#while [ $X -le 100 ]
#do
#	echo $X
#  mkdir 10x10_B4_run${X}
#	./interpret tissueGrowth2_10x10_B4.pah simulate 10 | cut -d' ' -f1,302-401 | ./outputConverter B4 ${X}
#	X=$((X+1))
#done

#X=1
#while [ $X -le 100 ]
#do
#	echo $X
#  mkdir 10x10_B3_run${X}
#	./interpret tissueGrowth2_10x10_B3.pah simulate 10 | cut -d' ' -f1,302-401 | ./outputConverter B3 ${X}
#	X=$((X+1))
#done

#X=1
#while [ $X -le 100 ]
#do
#	echo $X
#  mkdir 10x10_B2_run${X}
#	./interpret tissueGrowth2_10x10_B2.pah simulate 10 | cut -d' ' -f1,302-401 | ./outputConverter B2 ${X}
#	X=$((X+1))
#done

#X=1
#while [ $X -le 100 ]
#do
#	echo $X
#  mkdir 10x10_B1_run${X}
#	./interpret tissueGrowth2_10x10_B1.pah simulate 10 | cut -d' ' -f1,302-401 | ./outputConverter B1 ${X}
#	X=$((X+1))
#done

#mkdir 10x10_B6_run101
#mkdir 10x10_B5_run101
#mkdir 10x10_B4_run101
#./interpret tissueGrowth2_10x10_B6.pah simulate 10 | cut -d' ' -f1,302-401 | ./outputConverter B6 101
#./interpret tissueGrowth2_10x10_B5.pah simulate 10 | cut -d' ' -f1,302-401 | ./outputConverter B5 101
#./interpret tissueGrowth2_10x10_B4.pah simulate 10 | cut -d' ' -f1,302-401 | ./outputConverter B4 101

