(* Interpreter and Simulator for Process Algebra with Hooks *)
(* Andrea Degasperi ~ University of Glasgow *)

open Printf
open Multiset

(*---------------------------------------------*)
(*------------- Type definitions --------------*)
(*---------------------------------------------*)


type exp = Real of float 
              | VarName of string * exp list
              | Sum of exp * exp
              | Diff of exp * exp
              | Prod of exp * exp
              | Div of exp * exp
              | Power of exp * exp
              | Exp of exp
              | Log of exp
              | Sin of exp
              | Cos of exp;;

type action = Action of string * exp list;;

module Action_mset :
  sig
    type elt = action
    type t = elt multiset
    val empty: t
    val is_empty: t -> bool
    val mem: elt -> t -> bool
    val add: elt -> int -> t -> t
    val singleton: elt -> t
    val remove: elt -> int -> t -> t
    val union: t -> t -> t
    val inter: t -> t -> t
    val diff: t -> t -> t
    val duplicate : int -> t -> t
    val compare: t -> t -> int
    val equal: t -> t -> bool
    val subset: t -> t -> bool
    val iter: (elt -> int -> unit) -> t -> unit
    val fold: (elt -> int -> 'a -> 'a) -> t -> 'a -> 'a
    val multifold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
    val map: (elt -> elt) -> t -> t
    val for_all: (elt -> int -> bool) -> t -> bool
    val exists: (elt -> int -> bool) -> t -> bool
    val filter: (elt -> int -> bool) -> t -> t
    val partition: (elt -> int -> bool) -> t -> t * t
    val cardinal: t -> int
    val elements: t -> elt list
    val elements_packed: t -> (elt * int) list
    val min_elt: t -> elt
    val max_elt: t -> elt
    val choose: t -> elt
    val of_list: (elt * int) list -> t
  end;;


type agent = Agent of string * exp list;;

type bexp = True
            | False
            | Equal of exp * exp
            | LessThan of exp * exp
            | GreaterThan of exp * exp
            | LessEqualThan of exp * exp
            | GreaterEqualThan of exp * exp
            | NotEqualThan of exp * exp
            | And of bexp * bexp
            | Not of bexp
            | Or of bexp * bexp;;

type definition_process = Nil
                          | Choice of definition_process * definition_process
                          | Prefix of Action_mset.t * Action_mset.t * agent
                          | IfThenElse of bexp * definition_process * definition_process;;

type model_process = MAgent of agent
                      | Sync of Action_mset.t * model_process * model_process
                      | Syncv of Action_mset.t * model_process * model_process;;

(* in agent_definition we know that
  1. the agent is a name alone or a name followed by a list of VarName with empty explist
  2. the first exp is a VarName 
*)
type agent_definition = AgentDef of agent * exp * exp * definition_process;;

(* in a fun_rate_def the exp list is the list of participants, so a list of VarName. The last exp is the actual functional rate *)
type fun_rate_def = FunRateDef of action * exp list * exp;;

(* in assignment we know that exp is a VarName *)
type assignment = Assignment of exp * float;;


type line = LineAgentDef of agent_definition
            | LineAssignment of assignment
            | LineFunRate of fun_rate_def
            | LineModel of model_process;;

type code = EmptyCode
            | Code of code * line;;

type move = Move of Action_mset.t * Action_mset.t * assignment list * model_process;;

type rated_move = RatedMove of Action_mset.t * Action_mset.t * float * model_process;;

(* Printing *)

val printModelProcessCompact: model_process -> unit;;

val printModelProcessCompactWithSpaces: model_process -> unit;;

val printModelProcessMinimal: model_process -> unit;;

val printNextCompact: ((move list)) -> unit;;

val printMovesCompact: ((move list)) -> unit;;

val printRatedMovesCompact: ((rated_move list)) -> unit;;

(* Moves *)

exception MonteCarloException;;

val findMoves: (assignment list) -> ((agent_definition list)) -> (model_process) -> (move list);;

val splitMovesIntoOpenAndClosed: (fun_rate_def list) -> (move list) -> ((move list)*(move list));;

val rateClosedMoves: (fun_rate_def list) -> ((assignment list)) -> ((move list)) -> ((rated_move list))

val monteCarloStep: ((rated_move list)) -> ((float)*(model_process));;

val monteCarloLoop: ((assignment list)) -> ((agent_definition list)) -> (model_process) -> ((fun_rate_def list)) -> float -> float -> unit;;

