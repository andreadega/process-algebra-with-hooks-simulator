open AM;;
open Parser;;
open Lexer;;
type position = Lexing.position


let rec readFromParseTree (codeTree:code) (modelEnv:(assignment list)) (agentDefList:(agent_definition list)) (currentState:model_process) (funRateDefList:(fun_rate_def list)) =
  match codeTree with
    EmptyCode -> (modelEnv,agentDefList,currentState,funRateDefList)
  | Code(left,right) -> match right with
                          LineAgentDef(ad) -> (
                            readFromParseTree left modelEnv (ad::agentDefList) currentState funRateDefList
                          )
                        | LineAssignment(assingment) -> (
                            readFromParseTree left (assingment::modelEnv) agentDefList currentState funRateDefList
                          )
                        | LineFunRate(frd) -> (
                            readFromParseTree left modelEnv agentDefList currentState (frd::funRateDefList)
                          )
                        | LineModel(mc) -> (
                            readFromParseTree left modelEnv agentDefList mc funRateDefList
                          )
;;



(*--------------------------------------*)
(*---------- Data structures -----------*)
(*--------------------------------------*)

(* let stepsize = 0.5;; *)

(* I might have to use tau because of the strong typing *)
(* let tau = Action(Name("tau"),Location(Coord1(0)));; *)
(*
let rawUnProcessedLTS = Hashtbl.create 20;;
let idToModelComponentTable = Hashtbl.create 20;;
let modelComponentToIdTable = Hashtbl.create 20;;
let unProcessedLTS = Hashtbl.create 20;;
let processedLTS = Hashtbl.create 20;;
let minimisedLTS = Hashtbl.create 20;;
*)
(*-------------- use lexer and parser here -----------------*)

let format (pos:position) = 
    "Line " ^ string_of_int pos.Lexing.pos_lnum ^ 
    " char " ^ string_of_int (pos.Lexing.pos_cnum - pos.Lexing.pos_bol) ^ ": "

let result = let cin =
               if Array.length Sys.argv > 1
               then open_in Sys.argv.(1)
               else stdin
             in
             let lexbuf = Lexing.from_channel cin in
               try
                 Parser.code Lexer.token lexbuf
               with
                 Lexer.Illegal_character -> failwith (format (Lexing.lexeme_start_p lexbuf) ^ "Illegal character")
	             | Parsing.Parse_error -> failwith (format (Lexing.lexeme_start_p lexbuf) ^ "Syntax error");;


(*-------------------------------------------------------*)

let (modelEnv,agentDefList,currentState,funRateDefList) = readFromParseTree result [] [] (MAgent(Agent("Nil",[]))) [];;
(*
let oneStepMoves = findMoves modelEnv agentDefList currentState;;

print_string "Initial state:\n";;

printModelProcessCompact currentState;;

print_string "\n\nMoves:\n";;

printMovesCompact oneStepMoves;;

let (openMoves,closedMoves) = splitMovesIntoOpenAndClosed funRateDefList oneStepMoves;;

print_string "\n\nOpen Moves:\n";;

printMovesCompact openMoves;;

print_string "\n\nClosed Moves:\n";;

printMovesCompact closedMoves;;

let ratedMoves = rateClosedMoves funRateDefList modelEnv closedMoves;;

print_string "\n\nRated Moves:\n";;

printRatedMovesCompact ratedMoves;;

let (tau,currentState) = monteCarloStep ratedMoves;;

print_string "\n\nMonte Carlo Delay and Next State:\n";;

print_string "delay: "; print_float tau; print_string ", next: ";;

printModelProcessMinimal currentState; print_string "\n\n";;
*)

let printIncorrectOptions () = print_string ("Incorrect options, Use:\n\n\t ./interpreter FILENAME simulate ENDTIME\n"^"to simulate until ENDTIME; ENDTIME must be float\n\n\t or \n\n\t ./interpreter FILENAME next\n"^"to retrive the list of next states\n\n");;

type mutable_pair = { mutable tau: float; mutable newCurrentState: model_process };;
let update (p:mutable_pair) (pair:((float)*(model_process))) = match pair with (x,y) -> (p.tau <- x; p.newCurrentState <- y);;

if (Array.length Sys.argv = 4) then (
  if(Sys.argv.(2) = "simulate") then (

    let endtime = float_of_string Sys.argv.(3) in
    print_float 0.; print_string " "; printModelProcessCompactWithSpaces currentState; print_string "\n";
    let currentTime = ref 0. in
    let mutpair = { tau = 0.; newCurrentState = currentState } in
    while !currentTime <= endtime  do
      let oneStepMoves = findMoves modelEnv agentDefList (mutpair.newCurrentState) in
      let (openMoves,closedMoves) = splitMovesIntoOpenAndClosed funRateDefList oneStepMoves in
      let ratedMoves = rateClosedMoves funRateDefList modelEnv closedMoves in
      try (
        update mutpair (monteCarloStep ratedMoves);
        print_float (!currentTime +. mutpair.tau); print_string " "; printModelProcessCompactWithSpaces (mutpair.newCurrentState); print_string "\n";    
        currentTime := !currentTime +. mutpair.tau )
      with
        MonteCarloException -> currentTime := endtime +. 1.
    done; print_string "-1\n"
    
(*    monteCarloLoop modelEnv agentDefList currentState funRateDefList 0. endtime; print_string "-1\n"*)
  )else(
    printIncorrectOptions ()
  )
) else if (Array.length Sys.argv = 3) then (
  if(Sys.argv.(2) = "next") then (

    let oneStepMoves = findMoves modelEnv agentDefList currentState in
    let (openMoves,closedMoves) = splitMovesIntoOpenAndClosed funRateDefList oneStepMoves in
    let ratedMoves = rateClosedMoves funRateDefList modelEnv closedMoves in
    print_string "Initial state:\n";
    printModelProcessCompact currentState;
    print_string "\n\nMoves:\n";
    printMovesCompact oneStepMoves;
    print_string "\n\nOpen Moves:\n";
    printMovesCompact openMoves;
    print_string "\n\nClosed Moves:\n";
    printMovesCompact closedMoves;
    print_string "\n\nRated Moves:\n";
    printRatedMovesCompact ratedMoves

  )else (printIncorrectOptions ())
) else (printIncorrectOptions ())


(* OLD STUFF for reference *)


(*
computeRawUnProcessedLTS stepsize modelDefinition biochemicalActions seqCompDefList rawUnProcessedLTS;;

populateIdTables modelComponentToIdTable idToModelComponentTable rawUnProcessedLTS;;

computeUnProcessedLTS modelComponentToIdTable rawUnProcessedLTS unProcessedLTS;;
*)
(* relabelling *)

(* let selectedActions = [x_0; x_1; y_0; y_1];; *)
(* let selectedActions = [];; *)
(*
stochasticProcessing modelEnv selectedActions biochemicalActions funRateDef unProcessedLTS processedLTS;;
*)

(*
if (Array.length Sys.argv > 2) then (
  if(Sys.argv.(2) = "minimise") then (

    let activitiesProcessedLTS = getActivitiesProcessedLTS processedLTS in

    (* print_string (printActivityList activitiesProcessedLTS);;*)

    let bisimulationEquivalenceClasses =  bisimulationMinimisation activitiesProcessedLTS processedLTS in
    print_string ("/*\n"^(printEquivalenceClasses idToModelComponentTable bisimulationEquivalenceClasses)^"*/\n");

    buildMinimisedLTS activitiesProcessedLTS bisimulationEquivalenceClasses processedLTS minimisedLTS;

    printMinimisedLTS_dot_ID minimisedLTS
    
  ) else if (Sys.argv.(2) = "export") then (
    exportLTS processedLTS;
    exportIdMap idToModelComponentTable
  ) else (print_string ("/*\nUnknown option \""^Sys.argv.(2)^"\"\nPrinting Processed LTS:\n*/\n"); printStochLTS_dot_ID processedLTS)
) else printStochLTS_dot_ID processedLTS;;
*)

