(* Interpreter and Simulator for Process Algebra with Hooks *)
(* Andrea Degasperi ~ University of Glasgow *)

open Printf
open Multiset

(*---------------------------------------------*)
(*------------- Type definitions --------------*)
(*---------------------------------------------*)



type exp = Real of float 
              | VarName of string * exp list
              | Sum of exp * exp
              | Diff of exp * exp
              | Prod of exp * exp
              | Div of exp * exp
              | Power of exp * exp
              | Exp of exp
              | Log of exp
              | Sin of exp
              | Cos of exp;;

type action = Action of string * exp list;;

module Action_mset = Multiset.Make (struct
                                  type t = action
                                  let compare = compare
                                end);;


type agent = Agent of string * exp list;;

type bexp = True
            | False
            | Equal of exp * exp
            | LessThan of exp * exp
            | GreaterThan of exp * exp
            | LessEqualThan of exp * exp
            | GreaterEqualThan of exp * exp
            | NotEqualThan of exp * exp
            | And of bexp * bexp
            | Not of bexp
            | Or of bexp * bexp;;

type definition_process = Nil
                          | Choice of definition_process * definition_process
                          | Prefix of Action_mset.t * Action_mset.t * agent
                          | IfThenElse of bexp * definition_process * definition_process;;

type model_process = MAgent of agent
                      | Sync of Action_mset.t * model_process * model_process
                      | Syncv of Action_mset.t * model_process * model_process;;

(* in agent_definition we know that
  1. the agent is a name alone or a name followed by a list of VarName with empty explist
  2. the first exp is a VarName 
*)
type agent_definition = AgentDef of agent * exp * exp * definition_process;;

(* in a fun_rate_def the exp list is the list of participants, so a list of VarName. The last exp is the actual functional rate *)
type fun_rate_def = FunRateDef of action * exp list * exp;;

(* in assignment we know that exp is a VarName *)
type assignment = Assignment of exp * float;;


type line = LineAgentDef of agent_definition
            | LineAssignment of assignment
            | LineFunRate of fun_rate_def
            | LineModel of model_process;;

type code = EmptyCode
            | Code of code * line;;


type move_dp = MoveDP of Action_mset.t * Action_mset.t * bexp * agent;;

type move = Move of Action_mset.t * Action_mset.t * assignment list * model_process;;

type rated_move = RatedMove of Action_mset.t * Action_mset.t * float * model_process;;


(* Exceptions *)

exception RealExpected;;
exception AgentNotDefined;;
exception FunRateNotDefined;;
exception IncompatibleListsForEnv;;
exception VariableNotFound;;
exception VarNameExpected;;
exception MonteCarloException;;

(* --------- Print Functions -------------------------------------------------*)



let rec printKlistAux (delimiter:string) (klist: exp list) =
  match klist with
    [] -> print_string ""
  | [Real(f)] -> print_float f
  | Real(f)::xs -> print_float f; print_string delimiter; printKlistAux delimiter xs
  | _ -> raise RealExpected
;;

let printKlist (klist:exp list) =
  if List.length klist = 0 then print_string ""
  else (print_string "("; printKlistAux "," klist; print_string ")")
;;

let printKlistWithSpaces (klist:exp list) =
  if List.length klist = 0 then print_string ""
  else (print_string " "; printKlistAux " " klist; print_string " ")
;;

let rec printModelProcessCompact (mp:model_process) =
  match mp with
    MAgent(Agent(name,klist)) -> print_string name; printKlist klist
  | Sync(s,mp1,mp2) -> printModelProcessCompact mp1; print_string "[.]"; printModelProcessCompact mp2
  | Syncv(s,mp1,mp2) -> printModelProcessCompact mp1; print_string "<.>"; printModelProcessCompact mp2
;;

let rec printModelProcessMinimal (mp:model_process) =
  match mp with
    MAgent(Agent(name,klist)) -> print_string name
  | Sync(s,mp1,mp2) -> printModelProcessMinimal mp1; print_string " "; printModelProcessMinimal mp2
  | Syncv(s,mp1,mp2) -> printModelProcessMinimal mp1; print_string " "; printModelProcessMinimal mp2
;;

let rec printModelProcessCompactWithSpaces (mp:model_process) =
  match mp with
    MAgent(Agent(name,klist)) -> print_string name; printKlistWithSpaces klist
  | Sync(s,mp1,mp2) -> printModelProcessCompactWithSpaces mp1; printModelProcessCompactWithSpaces mp2
  | Syncv(s,mp1,mp2) -> printModelProcessCompactWithSpaces mp1; printModelProcessCompactWithSpaces mp2
;;

let rec printNextCompact (moves:(move list)) =
  match moves with
    [] -> print_string "\n"
  | Move(a,e,env,ma)::xs -> printModelProcessCompact ma; print_string "\n"; printNextCompact xs
;;

let printAction (a:action) =
  match a with
    Action(name,klist) -> print_string name; printKlist klist
;;

(* m is the multiplicity of a *)
let rec printMActions (a:action) (m:int) =
  match m with
    1 -> printAction a
  | _ -> printAction a; print_string ","; printMActions a (m-1)
;;

(* m is the multiplicity of a *)
let printActionSet_SetFold (a:action) (m:int) (i:int) = if i=m then (printMActions a m; i-m) else (printMActions a m; print_string ","; i-m);;

let printActionSet (a:(Action_mset.t)) = if (Action_mset.fold printActionSet_SetFold a (Action_mset.cardinal a)) =0 then print_string "" else print_string "" ;;

let printMoveCompact (m:move) = 
  match m with 
    Move(a,e,env,ma) -> printActionSet a; print_string "["; printActionSet e; print_string "] :: "; printModelProcessCompact ma; print_string "\n"
;;

let rec printMovesCompact (moves:(move list)) =
  match moves with
    [] -> print_string "\n"
  | x::xs -> printMoveCompact x; printMovesCompact xs
;;

let rec printEvaluatedDP (dp:definition_process) =
  match dp with
    Nil -> print_string "Nil"
  | Prefix(s1,s2,a) -> (printActionSet s1; print_string "["; printActionSet s2; print_string "]."; printModelProcessCompact (MAgent(a)))
  | Choice(dp1,dp2) -> ((printEvaluatedDP dp1); print_string " + "; (printEvaluatedDP dp2))
  | IfThenElse(b,dp1,dp2) -> (print_string "if "; print_string " then "; (printEvaluatedDP dp1); print_string " else "; (printEvaluatedDP dp2))
;;


let printRatedMoveCompact (m:rated_move) = 
  match m with 
    RatedMove(a,e,f,ma) -> print_string "("; printActionSet a; print_string "["; printActionSet e; print_string "],"; print_float f; print_string ")\t :: "; printModelProcessCompact ma; print_string "\n"
;;

let rec printRatedMovesCompact (moves:(rated_move list)) =
  match moves with
    [] -> print_string "\n"
  | x::xs -> printRatedMoveCompact x; printRatedMovesCompact xs
;;

(* -------------- End Print Functions ---------------------------------------------------- *)


(* ------------------------------------------ *)
(* -------Computing Moves ------------------- *)
(* ------------------------------------------ *)

let addToBExpDP (addition:bexp) (mdp:move_dp) = 
  match mdp with
    MoveDP(s1,s2,b,a) -> MoveDP(s1,s2,And(b,addition),a)
;;

let rec findMovesDP (dp:definition_process) =
  match dp with
    Nil -> []
  | Prefix(s1,s2,a) -> [MoveDP(s1,s2,True,a)]
  | Choice(dp1,dp2) ->  (findMovesDP dp1)@(findMovesDP dp2)
  | IfThenElse(b,dp1,dp2) -> (List.map (addToBExpDP b) (findMovesDP dp1))@(List.map (addToBExpDP (Not(b))) (findMovesDP dp2))
;;



let rec retrieveAgentDef (adl:(agent_definition list)) (name:string) =
  match adl with
    [] -> raise AgentNotDefined
  | AgentDef(Agent(n,plist),v1,v2,dp)::xs -> if name=n then AgentDef(Agent(n,plist),v1,v2,dp) else retrieveAgentDef xs name
;;

let rec buildEnvironmentAux (plist:(exp list)) (klist:(exp list)) acc = 
  match plist,klist with
    [],[] -> acc
  | (VarName(name,[]))::ps,(Real(f))::ks -> buildEnvironmentAux ps ks ((Assignment(VarName(name,[]),f))::acc)
  | _ -> raise IncompatibleListsForEnv
;;

let buildEnvironment (plist:(exp list)) (klist:(exp list)) = buildEnvironmentAux plist klist [];;


(* We assume that var1 and var are VarName(...) *)
let rec retrieveVal (al:(assignment list)) (var:exp) =
  match al with
    [] -> raise VariableNotFound
  | Assignment(var1,f)::als -> if var1=var then f else retrieveVal als var
;;

let rec evaluateExpression (env:(assignment list)) (e:exp) =
  match e with
    Real(f) -> f
  | VarName(name,el) -> retrieveVal env (VarName(name,List.map (wrapEvaluateExpression env) el))
  | Sum(e1,e2) -> (evaluateExpression env e1) +. (evaluateExpression env e2)
  | Diff(e1,e2) -> (evaluateExpression env e1) -. (evaluateExpression env e2)
  | Prod(e1,e2) -> (evaluateExpression env e1) *. (evaluateExpression env e2)
  | Div(e1,e2) -> (evaluateExpression env e1) /. (evaluateExpression env e2)
  | Power(e1,e2) -> (evaluateExpression env e1)**(evaluateExpression env e2)
  | Exp(e1) -> exp(evaluateExpression env e1)
  | Log(e1) -> log(evaluateExpression env e1)
  | Sin(e1) -> sin(evaluateExpression env e1)
  | Cos(e1) -> cos(evaluateExpression env e1)
and
(* need to wrap into Real(...) so that it is still an expression *)
wrapEvaluateExpression (env:(assignment list)) (e:exp) = Real(evaluateExpression env e);;


let rec evaluateBoolean (env:(assignment list)) (b:bexp) =
  match b with
    True -> true
  | False -> false
  | Equal(e1,e2) -> (evaluateExpression env e1) = (evaluateExpression env e2)
  | LessThan(e1,e2) -> (evaluateExpression env e1) < (evaluateExpression env e2)
  | GreaterThan(e1,e2) -> (evaluateExpression env e1) > (evaluateExpression env e2)
  | LessEqualThan(e1,e2) -> (evaluateExpression env e1) <= (evaluateExpression env e2)
  | GreaterEqualThan(e1,e2) -> (evaluateExpression env e1) >= (evaluateExpression env e2)
  | NotEqualThan(e1,e2) -> (evaluateExpression env e1) != (evaluateExpression env e2)
  | And(b1,b2) -> (evaluateBoolean env b1) && (evaluateBoolean env b2)
  | Not(b1) -> not (evaluateBoolean env b1)
  | Or(b1,b2) -> (evaluateBoolean env b1) || (evaluateBoolean env b2)
;;



let evaluateAction (env:(assignment list)) (a:action) = 
  match a with
    Action(name,el) -> Action(name,(List.map (wrapEvaluateExpression env) el))
;;

(* need to use this with Action_mset.fold because there is no Action_mset.map *)
let evaluateActionSetFold (env:(assignment list)) (a:action) (m:int) (aset:Action_mset.t) =
  match a with
    Action(name,el) -> Action_mset.add (Action(name,(List.map (wrapEvaluateExpression env) el))) m aset
;;

let evaluateAgent (env:(assignment list)) (a:agent) =
  match a with
    Agent(name,el) -> Agent(name,(List.map (wrapEvaluateExpression env) el))
;;

let rec evaluateDP (env:(assignment list)) (dp:definition_process) =
  match dp with
    Nil -> Nil
  | Prefix(s1,s2,a) -> Prefix((Action_mset.fold (evaluateActionSetFold env) s1 Action_mset.empty),(Action_mset.fold (evaluateActionSetFold env) s2 Action_mset.empty),evaluateAgent env a)
  | Choice(dp1,dp2) -> Choice(evaluateDP env dp1,evaluateDP env dp2)
  | IfThenElse(b,dp1,dp2) -> if evaluateBoolean env b then evaluateDP env dp1 else evaluateDP env dp2
;;



let evaluateVarName (env:(assignment list)) (a:exp) =
  match a with
    VarName(name,el) -> VarName(name,(List.map (wrapEvaluateExpression env) el))
  | _-> raise VarNameExpected
;;

(* We know here b is true because we have evaluated the definition process before computing the moves *)
let caseMAgentListMap (env:(assignment list)) (move:move_dp) =
  match move with
    MoveDP(s1,s2,b,a) -> Move(s1,s2,env,MAgent(a))
;;

(* case asynch left *)
let caseSyncListFoldAsynchLeft (actionSet:(Action_mset.t)) (m2:model_process) (mlist:move list) (m:move) =
  match m with
    Move(s1,s2,env,ma) ->
      if Action_mset.is_empty (Action_mset.inter s1 actionSet) then (Move(s1,s2,env,Sync(actionSet,ma,m2)))::mlist
      else mlist
;;

(* case asynch right *)
let caseSyncListFoldAsynchRight (actionSet:(Action_mset.t)) (m1:model_process) (mlist:move list) (m:move) =
  match m with
    Move(s1,s2,env,ma) ->
      if Action_mset.is_empty (Action_mset.inter s1 actionSet) then (Move(s1,s2,env,Sync(actionSet,m1,ma)))::mlist
      else mlist
;;

(* synchronisation via horizontal cooperation requires nested loop on moves of two model processes *)
let caseSyncListFoldSynchSecondLoop (actionSet:(Action_mset.t)) (m1:move) (mlist:move list) (m2:move) =
  match m1,m2 with
    Move(a1,h1,env1,ma1),Move(a2,h2,env2,ma2) ->
      if not (Action_mset.is_empty (Action_mset.inter a2 (Action_mset.inter a1 actionSet)))
      then (Move(Action_mset.diff (Action_mset.union a1 a2) (Action_mset.inter a1 a2),Action_mset.union h1 h2,env1@env2,Sync(actionSet,ma1,ma2)))::mlist
      else mlist
;;

let caseSyncListFoldSynch (actionSet:(Action_mset.t)) (m2list:move list) (mlist:move list) (m1:move) =
  List.fold_left (caseSyncListFoldSynchSecondLoop actionSet m1) mlist m2list
;;

(* Verical synchronisation requires also that all the moves of one of the two processes are checked for conditions *)

let condSyncvAsynch (l:(Action_mset.t)) (e:(Action_mset.t)) (m:move) =
  match m with
    Move(b,f,env,ma) -> not (Action_mset.subset b (Action_mset.inter l e) || Action_mset.equal b (Action_mset.inter l e))
;;

(* case vertical asynch left *)
let caseSyncvListFoldAsynchLeft (l:(Action_mset.t)) (m2:model_process) (movesMP2:move list) (mlist:move list) (m:move) =
  match m with
    Move(a,e,env,ma) ->
      if (Action_mset.is_empty (Action_mset.inter a l)) && (List.for_all (condSyncvAsynch l e) movesMP2) then (Move(a,e,env,Syncv(l,ma,m2)))::mlist
      else mlist
;;

(* case vertical asynch right *)
let caseSyncvListFoldAsynchRight (l:(Action_mset.t)) (m1:model_process) (movesMP1:move list) (mlist:move list) (m:move) =
  match m with
    Move(a,e,env,ma) ->
      if (Action_mset.is_empty (Action_mset.inter a l)) && (List.for_all (condSyncvAsynch l e) movesMP1) then (Move(a,e,env,Syncv(l,m1,ma)))::mlist
      else mlist
;;

(* synchronisation via vertical cooperation requires nested loop on moves of two model processes. The two cases (left to right and right to left) can be checked together *)

let condSyncvSynch (l:(Action_mset.t)) (e:(Action_mset.t)) (b:(Action_mset.t)) (m:move) =
  match m with
    Move(b2,f,env,ma) -> (not (Action_mset.subset b2 (Action_mset.inter l e) || Action_mset.equal b2 (Action_mset.inter l e))) || (not (Action_mset.cardinal b2 > Action_mset.cardinal b))
;;

let caseSyncvListFoldSynchSecondLoop (l:(Action_mset.t)) (m1:move) (movesMP1:move list) (movesMP2:move list) (mlist:move list) (m2:move) =
  match m1,m2 with
    Move(a,e,env1,ma1),Move(b,f,env2,ma2) -> 
      let bool1 = (Action_mset.subset b (Action_mset.inter l e) || Action_mset.equal b (Action_mset.inter l e)) && (List.for_all (condSyncvSynch l e b) movesMP2) in
      let bool2 = (Action_mset.subset a (Action_mset.inter l f) || Action_mset.equal a (Action_mset.inter l f)) && (List.for_all (condSyncvSynch l f a) movesMP1) in
      if bool1 && not bool2 then (Move(Action_mset.union a b,Action_mset.union (Action_mset.diff e b) f,env1,Syncv(l,ma1,ma2)))::mlist
      else if bool2 && not bool1 then (Move(Action_mset.union a b,Action_mset.union (Action_mset.diff f a) e,env2,Syncv(l,ma1,ma2)))::mlist
      else if bool1 && bool2 then (Move(Action_mset.union a b,Action_mset.union (Action_mset.diff f a) e,env2,Syncv(l,ma1,ma2)))::((Move(Action_mset.union a b,Action_mset.union (Action_mset.diff e b) f,env1,Syncv(l,ma1,ma2)))::mlist)
      else mlist
;;

let caseSyncvListFoldSynch (actionSet:(Action_mset.t)) (movesMP1:move list) (movesMP2:move list) (mlist:move list) (m1:move) =
  List.fold_left (caseSyncvListFoldSynchSecondLoop actionSet m1 movesMP1 movesMP2) mlist movesMP2
;;

let rec findMoves (env:(assignment list)) (adl:(agent_definition list)) (mp:model_process) =
  match mp with
    MAgent(Agent(name,klist)) -> (let tmp = (retrieveAgentDef adl name) in
      match tmp with 
        AgentDef(Agent(n,plist),v1,v2,dp) ->
          let env1 = env@(buildEnvironment plist klist) in
          let env2 = [Assignment((evaluateVarName env1 v1),(evaluateExpression env1 v2))] in
          let evaluatedDP = evaluateDP env1 dp in
          let movesDP = findMovesDP evaluatedDP in
          (* printEvaluatedDP evaluatedDP; print_string "\n\n";*) List.map (caseMAgentListMap env2) movesDP)
  | Sync(s,mp1,mp2) -> 
      let movesMP1 = findMoves env adl mp1 in
      let movesMP2 = findMoves env adl mp2 in     
      (* printMovesCompact movesMP1; printMovesCompact movesMP2;*) (List.fold_left (caseSyncListFoldAsynchLeft s mp2) [] movesMP1)@(List.fold_left (caseSyncListFoldAsynchRight s mp1) [] movesMP2)@(List.fold_left (caseSyncListFoldSynch s movesMP2) [] movesMP1)
  | Syncv(s,mp1,mp2) ->
      let movesMP1 = findMoves env adl mp1 in
      let movesMP2 = findMoves env adl mp2 in     
      (* printMovesCompact movesMP1; printMovesCompact movesMP2;*) (List.fold_left (caseSyncvListFoldAsynchLeft s mp2 movesMP2) [] movesMP1)@(List.fold_left (caseSyncvListFoldAsynchRight s mp1 movesMP1) [] movesMP2)@(List.fold_left (caseSyncvListFoldSynch s movesMP1 movesMP2) [] movesMP1)
;;

(* ------------------------------------------ *)
(* ------------------ Rating ---------------- *)
(* ------------------------------------------ *)

(* extracts a list of VarName from an environment (that is, assignment list) *)
let rec envVar (env:(assignment list)) =
  match env with
    [] -> []
  | Assignment(varname,f)::xs -> varname::(envVar xs)
;;

(* active actions, that is actions associated with a functional rate, in an ActionSet.
The action set contains evaluated actions with a klist, while a fun rate def contains
actions with plist. Here we don't need to evaluate participants and rate. Just count
how many active actions are there (name) without checking the parameters *)

let isActionNameInFunRateDef (name:string) (funRateDef:(fun_rate_def)) =
  match funRateDef with
    FunRateDef(Action(n,p),elist,e) -> if name = n then true else false
;;

let isActionNameInFunRateDefList (funRateDefList:(fun_rate_def list)) (name:string) = List.exists (isActionNameInFunRateDef name) funRateDefList;;

(* m is the multiplicity of a *)
let activeActionsFoldSet (funRateDefList:(fun_rate_def list)) (a:action) (m:int) (actionList:(action list)) =
  match a with
    Action(name,elist) -> if (isActionNameInFunRateDefList funRateDefList name) && m = 1
                          then (a::actionList) else actionList
;;

let activeActions (funRateDefList:(fun_rate_def list)) (actionSet:(Action_mset.t)) = Action_mset.fold (activeActionsFoldSet funRateDefList) actionSet [];;

(* return a functional rate definition associated with an action name  *)

let rec retrieveFunRateDef (funRateDefList:(fun_rate_def list)) (name:string) =
  match funRateDefList with
    [] -> raise FunRateNotDefined
  | (FunRateDef(Action(n,el),elist,exp) as frd)::xs -> if n=name then frd else retrieveFunRateDef xs name
;;

let rec inList s l =
  match l with
    [] -> false
  | h::t -> (h = s) || (inList s t);;

let rec listInList s1 s2 =
  match s1 with
    [] -> true
   | h::t -> (inList h s2) && (listInList t s2);;

let rec listEqual s1 s2 = listInList s1 s2 && ((List.length s1)=(List.length s2));;

(* function that returns the number of occurencies of an evaluated action
and associated environment in a list of moves *)

let activeActionOccurenciesListFold (a:action) (env:(assignment list)) (n:int) (m:move) =
  match m with
    Move(aset,hset,env1,mp) -> if Action_mset.mem a aset && env=env1 then n+1 else n
;;

let activeActionOccurencies (a:action) (env:(assignment list)) (mlist:(move list)) = List.fold_left (activeActionOccurenciesListFold a env) 0 mlist;;

(* get open moves and closed moves from a set of moves *)

let rec splitMovesIntoOpenAndClosed_TR (funRateDefList:(fun_rate_def list)) (moves:(move list)) pair:((move list)*(move list)) = 
  match pair with (openMoves,closedMoves) -> (
    match moves with
      [] -> (openMoves,closedMoves)
    | (Move(a,h,env1,mp) as m)::xs -> (
      let aa = activeActions funRateDefList a in
      if List.length aa = 1 then (
        let Action(name,klist) = List.hd aa in
        let FunRateDef(Action(n,plist),participants,frate) = retrieveFunRateDef funRateDefList name in
        let env2 = buildEnvironment plist klist in
        let evaluatedParticipants = List.map (evaluateVarName env2) participants in
        if (listEqual evaluatedParticipants (envVar env1)) then (
          splitMovesIntoOpenAndClosed_TR funRateDefList xs (openMoves,(m::closedMoves))
        ) else splitMovesIntoOpenAndClosed_TR funRateDefList xs (m::openMoves,closedMoves)
      ) else splitMovesIntoOpenAndClosed_TR funRateDefList xs (m::openMoves,closedMoves)
    )
  )
;;

let splitMovesIntoOpenAndClosed (funRateDefList:(fun_rate_def list)) (moves:move list) =
  splitMovesIntoOpenAndClosed_TR funRateDefList moves ([],[]);;

(* rate moves *)

let rec rateClosedMoves_TR (funRateDefList:(fun_rate_def list)) (env:(assignment list)) (closedMoves:(move list)) (remainingClosedMoves:(move list)) (ratedMoves:(rated_move list)) =
  match remainingClosedMoves with
    [] -> ratedMoves
  | (Move(a,h,env1,mp))::xs -> let aa = activeActions funRateDefList a in
        let Action(name,klist) = List.hd aa in
        let FunRateDef(Action(n,plist),participants,frate) = retrieveFunRateDef funRateDefList name in
        let env2 = buildEnvironment plist klist in
        let k = evaluateExpression (env@env1@env2) frate in
        let r = k /. (float_of_int (activeActionOccurencies (Action(name,klist)) env1 closedMoves)) in
        rateClosedMoves_TR funRateDefList env closedMoves xs ((RatedMove(a,h,r,mp))::ratedMoves)
;;

let rateClosedMoves (funRateDefList:(fun_rate_def list)) (env:(assignment list)) (closedMoves:(move list)) = rateClosedMoves_TR funRateDefList env closedMoves closedMoves [];;


(* the following function takes a list of rated moves and returns a 
time delay and new current state, i.e. model process *)

let rec sumRates_TR (ratedMoves:(rated_move list)) (sum:float) =
  match ratedMoves with
    [] -> sum
  | RatedMove(a,h,f,mp)::xs -> sumRates_TR xs (f +. sum)
;;

let sumRates (ratedMoves:(rated_move list)) = sumRates_TR ratedMoves 0.;;

let rec chooseNextState_Aux (ratedMoves:(rated_move list)) (threshold:float) (carry:float) =
  match ratedMoves with
    [] -> raise MonteCarloException
  | RatedMove(a,h,f,mp)::xs -> (* print_float (carry +. f); print_float (threshold);*) if carry +. f >= threshold then mp else chooseNextState_Aux xs threshold (carry +. f)
;;


let chooseNextState (ratedMoves:(rated_move list)) (threshold:float) = chooseNextState_Aux ratedMoves threshold 0.;;

let monteCarloStep (ratedMoves:(rated_move list)) = Random.self_init ();
                      let r1 = Random.float 1. in
                      let r2 = Random.float 1. in
                      let sumOfRates = sumRates ratedMoves in
                      (* print_float sumOfRates; *) let tau = 1. /. (sumOfRates) *. log(1. /. r1) in
                      (tau,chooseNextState ratedMoves (sumOfRates *. r2))
;;

let rec monteCarloLoop (modelEnv:(assignment list)) (agentDefList:(agent_definition list)) (currentState:model_process) (funRateDefList:(fun_rate_def list)) (currentTime:float) (endTime:float) = 
  let oneStepMoves = findMoves modelEnv agentDefList currentState in
  let (openMoves,closedMoves) = splitMovesIntoOpenAndClosed funRateDefList oneStepMoves in
  let ratedMoves = rateClosedMoves funRateDefList modelEnv closedMoves in
  let (tau,newCurrentState) = monteCarloStep ratedMoves in
  print_float (currentTime +. tau); print_string " "; printModelProcessMinimal newCurrentState; print_string "\n";
  if currentTime +. tau >= endTime then () else (
    try
      monteCarloLoop modelEnv agentDefList newCurrentState funRateDefList (currentTime +. tau) endTime
    with
      MonteCarloException -> ()
  )
;;














