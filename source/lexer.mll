{
  open AM
  open Parser
  exception Eof
  exception Illegal_character
  

  let incr_linenum lexbuf =
    let pos = lexbuf.Lexing.lex_curr_p in
    lexbuf.Lexing.lex_curr_p <- { pos with
      Lexing.pos_lnum = pos.Lexing.pos_lnum + 1;
      Lexing.pos_bol = pos.Lexing.pos_cnum;
    }
  ;;
}

let digit = ['0'-'9']
let id = ['a'-'z' '_' 'A'-'Z']['a'-'z' 'A'-'Z' '_' '0'-'9']*

rule token = parse
  | [' ' '\t' ]	                    { token lexbuf }
  | '\n'	                          { incr_linenum lexbuf; token lexbuf }
  | digit+ as integer               { INTEGER (int_of_string integer)}
  | digit+ '.' digit* as floatnum   { FLOAT (float_of_string floatnum)}
  | "agent"                         { AGENT }
  | "model"                         { MODEL }
  | "const"                         { CONST }
  | "rate"                          { RATE }
  | "sin"                           { SIN }
  | "cos"                           { COS } 
  | "nil"                           { NIL }
  | "log"                           { LOG } 
  | "exp"                           { EXP }
  | "if"                            { IF }
  | "then"                          { THEN }
  | "else"                          { ELSE }
  | "true"                          { TRUE }
  | "false"                         { FALSE }
  | id as identifier                { IDENTIFIER (identifier) }
  | "&&"                            { AND }
  | "||"                            { OR }
  | "=="                            { EQUAL }
  | "<="                            { LEQUAL }
  | ">="                            { GEQUAL }
  | "!="                            { NEQUAL }
  | '='                             { ASSIGN }
  | '!'                             { NOT }
  | '('                             { LPAR }
  | ')'                             { RPAR }
  | '['                             { LSQUARED }
  | ']'                             { RSQUARED }
  | '<'                             { LANGLE }
  | '>'                             { RANGLE }
  | ';'                             { SEMICOLON }
  | ':'                             { COLON }
  | '+'                             { PLUS }
  | '-'		                          { MINUS }
  | '*'		                          { MULTIPLY }
  | '/'		                          { DIVIDE }
  | '^'		                          { CARET }
  | ','                             { COMMA }
  | '.'                             { DOT }
  | eof                             { EOF }
  | _ 			                        { raise Illegal_character }


