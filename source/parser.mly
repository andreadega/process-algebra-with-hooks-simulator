%{
open AM

%}

%token <int> INTEGER
%token <float> FLOAT
%token <string> IDENTIFIER
%token PLUS MINUS MULTIPLY DIVIDE CARET SIN COS TRUE FALSE
%token SEMICOLON COLON AGENT MODEL ASSIGN EQUAL LEQUAL GEQUAL NEQUAL
%token CONST RATE LPAR RPAR LSQUARED RSQUARED EOF AND OR NOT
%token LANGLE RANGLE COMMA NIL LOG EXP DOT
%token IF THEN ELSE
%left IF THEN ELSE
%left PLUS MINUS
%left MULTIPLY DIVIDE
%left CARET AND OR  EQUAL LEQUAL GEQUAL NEQUAL
%left LSQUARED RSQUARED LANGLE RANGLE ASSIGN COMMA LPAR RPAR
%nonassoc SIN COS LOG EXP NOT

%start code
%type <AM.code> code

%%

expr: LPAR expr RPAR              { $2 }
      | IDENTIFIER                { VarName($1,[]) }
      | FLOAT                     { Real($1) }
      | INTEGER                   { Real(float_of_int $1) }
      | expr PLUS expr            { Sum($1,$3) }
      | expr MINUS expr           { Diff($1,$3) }
      | expr MULTIPLY expr        { Prod($1,$3) }
      | expr DIVIDE expr          { Div($1,$3) }
      | expr CARET expr           { Power($1,$3) }
      | SIN expr                  {Sin($2)}
      | COS expr                  {Cos($2)}
      | LOG expr                  {Log($2)}
      | EXP expr                  {Exp($2)}
;

explist: expr                                    { [$1] }
          | expr COMMA explist                   { $1::$3 }
;

action: IDENTIFIER                                                 { Action($1,[]) }
        | IDENTIFIER LPAR explist RPAR                            { Action($1,$3) }
;

actionlist: action                                                 { Action_mset.singleton $1 }
            | action COMMA actionlist                              { Action_mset.add $1 1 $3 }
;

agent: IDENTIFIER                                                 { Agent($1,[]) }
        | IDENTIFIER LPAR explist RPAR                            { Agent($1,$3) }
;

paramlist: IDENTIFIER                             { [VarName($1,[])] }
            | IDENTIFIER COMMA paramlist          { (VarName($1,[]))::$3 }
;

pagent: IDENTIFIER                                                 { Agent($1,[]) }
        | IDENTIFIER LPAR paramlist RPAR                            { Agent($1,$3) }
;

klist: INTEGER                                    { [Real(float_of_int $1)] }
        | FLOAT                                   { [Real($1)] }
        | INTEGER COMMA klist                     { (Real(float_of_int $1))::$3 }
        | FLOAT COMMA klist                       { (Real($1))::$3 }
;

kagent: IDENTIFIER                                              { Agent($1,[]) }
        | IDENTIFIER LPAR klist RPAR                            { Agent($1,$3) }
;

bexpr: LPAR bexpr RPAR                                          { $2 }
        | TRUE                                                  { True }
        | FALSE                                                 { False }
        | expr EQUAL expr                                       { Equal($1,$3) }
        | expr LANGLE expr                                      { LessThan($1,$3) }
        | expr RANGLE expr                                      { GreaterThan($1,$3) }
        | expr LEQUAL expr                                      { LessEqualThan($1,$3) }
        | expr GEQUAL expr                                      { GreaterEqualThan($1,$3) }
        | expr NEQUAL expr                                      { NotEqualThan($1,$3) }
        | NOT bexpr                                             { Not($2) }
        | bexpr AND bexpr                                       { And($1,$3) }
        | bexpr OR bexpr                                        { Or($1,$3) }

def_proc: NIL                                                   { Nil }
          | LPAR def_proc RPAR                                  { $2 }
          | def_proc PLUS def_proc                              { Choice($1,$3) }
          | actionlist DOT agent                                { Prefix($1,Action_mset.empty,$3) }
          | actionlist LSQUARED actionlist RSQUARED DOT agent   { Prefix($1,$3,$6) }
          | IF bexpr THEN def_proc ELSE def_proc                { IfThenElse($2,$4,$6) }
;

model_proc: kagent                                                { MAgent($1) }
            | LPAR model_proc RPAR                                { $2 }
            | model_proc LSQUARED actionlist RSQUARED model_proc  { Sync($3,$1,$5) }
            | model_proc LSQUARED RSQUARED model_proc             { Sync(Action_mset.empty,$1,$4) }
            | model_proc LANGLE actionlist RANGLE model_proc      { Syncv($3,$1,$5) }
            | model_proc LANGLE RANGLE model_proc                 { Syncv(Action_mset.empty,$1,$4) }
;

fexpr: LPAR fexpr RPAR                  { $2 }
      | IDENTIFIER                      { VarName($1,[]) }
      | IDENTIFIER LPAR explist RPAR    { VarName($1,$3) }
      | FLOAT                           { Real($1) }
      | INTEGER                         { Real(float_of_int $1) }
      | fexpr PLUS fexpr                { Sum($1,$3) }
      | fexpr MINUS fexpr               { Diff($1,$3) }
      | fexpr MULTIPLY fexpr            { Prod($1,$3) }
      | fexpr DIVIDE fexpr              { Div($1,$3) }
      | fexpr CARET fexpr               { Power($1,$3) }
      | SIN fexpr                       { Sin($2) }
      | COS fexpr                       { Cos($2) }
      | LOG fexpr                       { Log($2) }
      | EXP fexpr                       { Exp($2) }
;

participants_list: IDENTIFIER                                              { [VarName($1,[])] }
                    | IDENTIFIER LPAR explist RPAR                         { [VarName($1,$3)] }
                    | IDENTIFIER COMMA participants_list                   { (VarName($1,[]))::$3 }
                    | IDENTIFIER LPAR explist RPAR COMMA participants_list { (VarName($1,$3))::$6 }
;


line: AGENT pagent COMMA LSQUARED IDENTIFIER COMMA expr RSQUARED ASSIGN def_proc SEMICOLON { LineAgentDef(AgentDef($2,VarName($5,[]),$7,$10)) }
      | AGENT pagent COMMA LSQUARED IDENTIFIER LPAR explist RPAR COMMA expr RSQUARED ASSIGN def_proc SEMICOLON { LineAgentDef(AgentDef($2,VarName($5,$7),$10,$13)) }
      | MODEL model_proc SEMICOLON                { LineModel($2) }
      | CONST IDENTIFIER ASSIGN INTEGER SEMICOLON  { LineAssignment(Assignment(VarName($2,[]),float_of_int $4)) }
      | CONST IDENTIFIER ASSIGN FLOAT SEMICOLON    { LineAssignment(Assignment(VarName($2,[]),$4)) }
      | RATE action COMMA LSQUARED participants_list RSQUARED ASSIGN fexpr SEMICOLON          { LineFunRate(FunRateDef($2,$5,$8)) }
;
coded: line                                       { Code(EmptyCode,$1) }
      | coded line                                { Code($1,$2)}
;
code: coded EOF                                   { $1 }
;

