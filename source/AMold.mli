(* Abstract Machine for Process Algebra with Hooks *)
(* Andrea Degasperi ~ University of Glasgow *)

open Printf

(*---------------------------------------------*)
(*------------- Type definitions --------------*)
(*---------------------------------------------*)


type name = Name of string;;

type position = Coord3 of int * int * int
              | Coord2 of int * int 
              | Coord1 of int;;

type location = Location of position
              | Transport of position * position;;

type action = Action of name * location;;

type constant = Const of string * position;;

type sequential_component = Nil 
                          | Prefix of action list * action list * constant 
                          | Choice of sequential_component * sequential_component;;

type model_component = Sync of action list * model_component * model_component
                     | Listen of action list * model_component * constant
                     | ConstModel of constant;;

type sequential_component_definition = SeqComp of constant * string * int * sequential_component;;

type assignment = Assignment of string * float;;

type raw_transition = RawTransition of action list * action list * assignment list * model_component;;

type transition = Transition of action list * action list * assignment list * int;;

type stoch_transition = StochTransition of action list * float * int;;

type minimised_transition = MinimisedTransition of action list * float * int list;;

type fun_rate = Real of float 
              | VarName of string
              | Sum of fun_rate * fun_rate
              | Diff of fun_rate * fun_rate
              | Prod of fun_rate * fun_rate
              | Div of fun_rate * fun_rate
              | Power of fun_rate * fun_rate
              | Exp of fun_rate
              | Log of fun_rate
              | Sin of fun_rate
              | Cos of fun_rate;;

type fun_rate_def = FunRateDef of action * fun_rate;;

type stepsize = StepSize of float;;

type line = LineSeqComp of sequential_component_definition
            | LineAssignment of assignment
            | LineFunRate of fun_rate_def
            | LineModel of model_component
            | LineStepSize of stepsize
            | LineSelect of action list
            | LineBiochemical of action list;;

type code = EmptyCode
            | Code of code * line;;


(*----------------------------------------------*)
(*------------- Global Parameters --------------*)
(*----------------------------------------------*)


(*--------------------------------------*)
(*----------- Print Functions ----------*)
(*--------------------------------------*)

(* The following function prints the entire LTS *)
val printLTS: (((model_component, (raw_transition list)) Hashtbl.t) ) -> unit;; 

(*------------------------------------------------*)
(*-------------- Export Dot File -----------------*)
(*------------------------------------------------*)

val printStatesList: ((model_component list)) -> string;;

val printLTS_dot: (((model_component, (raw_transition list)) Hashtbl.t)) -> unit;;

val printRelation: (((int, model_component) Hashtbl.t)) -> (((int * int) list)) -> string;;

val printEquivalenceClasses: (((int, model_component) Hashtbl.t)) -> ((int list list)) -> string;;

(*------ Export Dot File of Stochastic LTS --------*)

val printStochLTS_dot: (((int, model_component) Hashtbl.t)) -> (((int,stoch_transition list) Hashtbl.t)) -> unit;;

(* Stochastic LTS, dot file, just IDs *)

val printStochLTS_dot_ID: (((int,stoch_transition list) Hashtbl.t)) -> unit;;

(* Print Minimised LTS, just IDs *)

val printMinimisedLTS_dot_ID: (((int list,minimised_transition list) Hashtbl.t)) -> unit;;

val exportLTS: (((int,stoch_transition list) Hashtbl.t)) -> unit;;

val exportIdMap: (((int, model_component) Hashtbl.t)) -> unit;;

(*-----------------------------------------------*)
(*------- Abstract Machine main functions -------*)
(*-----------------------------------------------*)

val computeRawUnProcessedLTS: float -> (model_component) -> ((action list)) -> ((sequential_component_definition list)) -> (((model_component,(raw_transition list)) Hashtbl.t)) -> unit;;

(* Substitute model components with integers IDs *)

val populateIdTables: (((model_component, int) Hashtbl.t)) -> (((int, model_component) Hashtbl.t)) -> (((model_component, raw_transition list) Hashtbl.t)) -> int;;

val computeUnProcessedLTS: (((model_component, int) Hashtbl.t)) -> (((model_component, raw_transition list) Hashtbl.t)) -> (((int, transition list) Hashtbl.t)) -> unit;;


(*--------------------------------------*)
(*--------- Label Processing -----------*)
(*--------------------------------------*)


val stochasticProcessing: (assignment list) -> ((action list)) -> ((action list)) -> ((fun_rate_def list)) -> (((int,(transition list)) Hashtbl.t)) -> (((int,(stoch_transition list)) Hashtbl.t)) -> unit;;


(*--------------------------------------*)
(*---------- Bisimulation --------------*)
(*--------------------------------------*)


val getActivitiesProcessedLTS: (((int,(stoch_transition list)) Hashtbl.t)) -> (action list list);;

val getStatesProcessedLTS: (((int,(stoch_transition list)) Hashtbl.t)) -> (int list);;

val bisimulationMinimisation: ((action list list)) -> (((int,(stoch_transition list)) Hashtbl.t)) -> int list list ;;

val buildMinimisedLTS: ((action list list)) -> ((int list list)) -> (((int,(stoch_transition list)) Hashtbl.t)) -> (((int list,(minimised_transition list)) Hashtbl.t)) -> unit;;


