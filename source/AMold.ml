(* Abstract Machine for Process Algebra with Hooks *)
(* Andrea Degasperi ~ University of Glasgow *)

open Printf

(*---------------------------------------------*)
(*------------- Type definitions --------------*)
(*---------------------------------------------*)


type name = Name of string;;

type position = Coord3 of int * int * int
              | Coord2 of int * int 
              | Coord1 of int;;

type location = Location of position
              | Transport of position * position;;

type action = Action of name * location;;

type constant = Const of string * position;;
(*TODO use sets also for activities! or at least make it that x,y = y,x*)
type sequential_component = Nil 
                          | Prefix of action list * action list * constant 
                          | Choice of sequential_component * sequential_component;;

type model_component = Sync of action list * model_component * model_component
                     | Listen of action list * model_component * constant
                     | ConstModel of constant;;

type sequential_component_definition = SeqComp of constant * string * int * sequential_component;;

type assignment = Assignment of string * float;;

type raw_transition = RawTransition of action list * action list * assignment list * model_component;;

type transition = Transition of action list * action list * assignment list * int;;

type stoch_transition = StochTransition of action list * float * int;;

type minimised_transition = MinimisedTransition of action list * float * int list;;

type fun_rate = Real of float 
              | VarName of string
              | Sum of fun_rate * fun_rate
              | Diff of fun_rate * fun_rate
              | Prod of fun_rate * fun_rate
              | Div of fun_rate * fun_rate
              | Power of fun_rate * fun_rate
              | Exp of fun_rate
              | Log of fun_rate
              | Sin of fun_rate
              | Cos of fun_rate;;

type fun_rate_def = FunRateDef of action * fun_rate;;

type stepsize = StepSize of float;;

type line = LineSeqComp of sequential_component_definition
            | LineAssignment of assignment
            | LineFunRate of fun_rate_def
            | LineModel of model_component
            | LineStepSize of stepsize
            | LineSelect of action list
            | LineBiochemical of action list;;

type code = EmptyCode
            | Code of code * line;;


(*----------------------------------------------*)
(*------------- Global Parameters --------------*)
(*----------------------------------------------*)
(*
let h = 0.5;;
*)

(*--------------------------------------*)
(*----------- Print Functions ----------*)
(*--------------------------------------*)

let printPosition (pos:position) =
  match pos with
    Coord3(x,y,z) -> "("^(string_of_int x)^","^(string_of_int y)^","^(string_of_int z)^")"
  | Coord2(x,y) -> "("^(string_of_int x)^","^(string_of_int y)^")"
  | Coord1(x) -> "("^(string_of_int x)^")";;

let printLocation (loc:location) = 
  match loc with
    Location(pos) -> printPosition pos
  | Transport(pos1,pos2) -> "("^(printPosition pos1)^"->"^(printPosition pos2)^")";;

let rec printActivityAux (activity:(action list)) =
  match activity with
    [] -> ""
  | Action(Name(name),loc)::t -> let temp = printActivityAux t in 
                                 if temp = "" then name^":"^(printLocation loc) else name^":"^(printLocation loc)^","^temp;;

let printActivity (activity:(action list)) = if activity = [] then "tau" else printActivityAux activity;;


let rec printActivityList (activityList:(action list list)) = 
  match activityList with
    [] -> ""
  | h::t -> (printActivity h)^"\n"^(printActivityList t);;


let rec printEnvironmentAux (env:(assignment list)) =
  match env with
    [] -> ""
  | Assignment(varname,value)::t -> let temp = printEnvironmentAux t in 
                                 if temp = "" then "("^varname^","^(string_of_float value)^")" else "("^varname^","^(string_of_float value)^"),"^temp;;

let printEnvironment (env:(assignment list)) = "{"^(printEnvironmentAux env)^"}";;

let printConstant (constant:(constant)) =
  match constant with
    Const(name,position) -> name^":"^(printPosition position);;

let rec printModel (model:(model_component)) =
  match model with
    Sync(actionlist,model_component1,model_component2) -> "("^(printModel model_component1)^")["^(printActivity actionlist)^"]("^(printModel model_component2)^")"
  | Listen(actionlist,model_component,constant) -> "("^(printModel model_component)^")["^(printActivity actionlist)^"]("^(printConstant constant)^")"
  | ConstModel(constant) -> printConstant constant;;

let rec printTransitionsAux (transitions:(raw_transition list)) =
  match transitions with
    [] -> ""
  | RawTransition(act,hook,env,model)::t -> let temp = printTransitionsAux t in
                                         if temp ="" then "("^(printActivity act)^"["^(printActivity hook)^"],"^(printEnvironment env)^","^(printModel model)^")" else "("^(printActivity act)^"["^(printActivity hook)^"],"^(printEnvironment env)^","^(printModel model)^");\n"^(temp);;

let printTransitions (transitions:(raw_transition list)) = "{"^(printTransitionsAux transitions)^"}\n";;

let printOneStepTrasitions (a:(model_component)) (b:(raw_transition list)) = print_string ((printModel a)^"\n");
  match b with
    [] -> print_string ""
  | t -> print_string (printTransitions t);;

(* The following function prints the entire LTS *)
let printLTS (hashTable:((model_component, (raw_transition list)) Hashtbl.t) ) =  Hashtbl.iter printOneStepTrasitions hashTable;; 


(*------------------------------------------------*)
(*-------------- Export Dot File -----------------*)
(*------------------------------------------------*)


let rec printModelCompact (model:model_component) = 
  match model with
    Sync(actionlist,model_component1,model_component2) -> (printModelCompact model_component1)^(printModelCompact model_component2)
  | Listen(actionlist,model_component,constant) -> (printModelCompact model_component)^(printConstant constant)
  | ConstModel(constant) -> printConstant constant;;


let rec printStatesList (statesList:(model_component list)) = 
  match statesList with
    [] -> ""
  | h::t -> (printModelCompact h)^"\n"^(printStatesList t);;

let rec printTransitionsDotAux (a:model_component) (transitions:(raw_transition list)) =
  match transitions with
    [] -> ""
  | RawTransition(act,hook,env,model)::t -> "\""^(printModelCompact a)^"\" -> \""^(printModelCompact model)^"\" [ label=\""^(printActivity act)^"["^(printActivity hook)^"],"^(printEnvironment env)^"\" ];\n"^(printTransitionsDotAux a t);;

let printOneStepTrasitionsDot (a:model_component) (b:(raw_transition list)) = 
  match b with
    [] -> print_string ""
  | t -> print_string (printTransitionsDotAux a t);;


let printLTS_dot (hashTable:((model_component, (raw_transition list)) Hashtbl.t)) = print_string "digraph R {\n size=\"80,50\";\n orientation=land;\n node [shape = circle];\n";
Hashtbl.iter printOneStepTrasitionsDot hashTable;
print_string "}\n";;



let rec printRelation (idToModelComponentTable:((int, model_component) Hashtbl.t)) (pairsList:((int * int) list)) =
  match pairsList with
    [] -> ""
  | (a,b)::t -> "("^(printModelCompact (Hashtbl.find idToModelComponentTable a))^","^(printModelCompact (Hashtbl.find idToModelComponentTable b))^")\n"^(printRelation idToModelComponentTable t);;

let rec printEquivalenceClass (idToModelComponentTable:((int, model_component) Hashtbl.t)) (c:(int list)) =
  match c with
    [] -> ""
  | h::t -> (printModelCompact (Hashtbl.find idToModelComponentTable h))^"\n"^(printEquivalenceClass idToModelComponentTable t);;

let rec printEquivalenceClasses (idToModelComponentTable:((int, model_component) Hashtbl.t)) (equivalenceClasses:(int list list)) =
  match equivalenceClasses with 
    [] -> ""
  | h::t -> "Class:\n"^(printEquivalenceClass idToModelComponentTable h)^"------------\n"^printEquivalenceClasses idToModelComponentTable t;;


(*------ Export Dot File of Stochastic LTS --------*)

let rec printStochTransitionsDotAux (idToModelComponentTable:((int, model_component) Hashtbl.t)) (a:int) (transitions:(stoch_transition list)) =
  match transitions with
    [] -> ""
  | StochTransition(act,rate,model)::t -> "\""^(printModelCompact (Hashtbl.find idToModelComponentTable a))^"\" -> \""^(printModelCompact (Hashtbl.find idToModelComponentTable model))^"\" [ label=\""^(printActivity act)^","^(string_of_float rate)^"\" ];\n"^(printStochTransitionsDotAux (idToModelComponentTable:((int, model_component) Hashtbl.t)) a t);;


let printOneStepStochTrasitionsDot (idToModelComponentTable:((int, model_component) Hashtbl.t)) (a:int) (b:(stoch_transition list)) = 
  match b with
    [] -> print_string ""
  | t -> print_string (printStochTransitionsDotAux (idToModelComponentTable:((int, model_component) Hashtbl.t)) a t);;


let printStochLTS_dot (idToModelComponentTable:((int, model_component) Hashtbl.t)) (hashTable:((int,stoch_transition list) Hashtbl.t)) = print_string "digraph R {\n size=\"80,50\";\n orientation=land;\n node [shape = circle];\n";
Hashtbl.iter (printOneStepStochTrasitionsDot (idToModelComponentTable:((int, model_component) Hashtbl.t))) hashTable;
print_string "}\n";;

(* Stochastic LTS, dot file, just IDs *)

let rec printStochTransitionsDotAux_ID (a:int) (transitions:(stoch_transition list)) =
  match transitions with
    [] -> ""
  | StochTransition(act,rate,model)::t -> "\""^(string_of_int a)^"\" -> \""^(string_of_int model)^"\" [ label=\""^(printActivity act)^","^(string_of_float rate)^"\" ];\n"^(printStochTransitionsDotAux_ID a t);;


let printOneStepStochTrasitionsDot_ID (a:int) (b:(stoch_transition list)) = 
  match b with
    [] -> print_string ""
  | t -> print_string (printStochTransitionsDotAux_ID a t);;


let printStochLTS_dot_ID (processedLTS:((int,stoch_transition list) Hashtbl.t)) = print_string "digraph R {\n size=\"80,50\";\n orientation=land;\n node [shape = circle];\n";
Hashtbl.iter printOneStepStochTrasitionsDot_ID processedLTS;
print_string "}\n";;

(* Print Minimised LTS, just IDs *)

let rec printEquivalenceClassIDs (equivalenceClass:(int list)) =
  match equivalenceClass with
    [] -> ""
  | h::t -> let temp = printEquivalenceClassIDs t in
            if temp = "" then string_of_int h else (string_of_int h)^","^(temp);;

let rec printMinimisedTransitionsDotAux_ID (a:int list) (transitions:(minimised_transition list)) =
  match transitions with
    [] -> ""
  | MinimisedTransition(act,rate,model)::t -> "\""^(printEquivalenceClassIDs a)^"\" -> \""^(printEquivalenceClassIDs model)^"\" [ label=\""^(printActivity act)^","^(string_of_float rate)^"\" ];\n"^(printMinimisedTransitionsDotAux_ID a t);;

let rec printEquivalenceClassesIDs (equivalenceClasses:(int list list)) =
  match equivalenceClasses with 
    [] -> ""
  | h::t -> "Class:\n"^(printEquivalenceClassIDs h)^"------------\n"^printEquivalenceClassesIDs t;;


let printOneStepMinimisedTrasitionsDot_ID (a:int list) (b:(minimised_transition list)) = 
  match b with
    [] -> print_string ""
  | t -> print_string (printMinimisedTransitionsDotAux_ID a t);;

(* (* debug function *)
let printOneStepMinimisedTrasitionsDot_ID (a:int list) (b:(minimised_transition list)) = 
  match a with
    [] -> print_string "empty class\n"
  | c::cs -> print_string "print transitions of class "; print_int c; print_string "\n";
  (match b with
    [] -> print_string "no transitions in this class\n"
  | t -> print_string (printMinimisedTransitionsDotAux_ID a t));;
*)


let printMinimisedLTS_dot_ID (minimisedLTS:((int list,minimised_transition list) Hashtbl.t)) = print_string "digraph R {\n size=\"80,50\";\n orientation=land;\n node [shape = circle];\n";
Hashtbl.iter printOneStepMinimisedTrasitionsDot_ID minimisedLTS;
print_string "}\n";;

(* export to file *)

let rec exportLTS_TrasitionsAux oc (a:int) (transitions:(stoch_transition list)) =
  match transitions with
    [] -> ""
  | StochTransition(act,rate,model)::t -> fprintf oc "%s %s %s %s\n" (string_of_int a) (printActivity act) (string_of_float rate) (string_of_int model); (exportLTS_TrasitionsAux oc a t);;


let exportLTS_Trasitions oc (a:int) (b:(stoch_transition list)) = 
  match b with
    [] -> print_string ""
  | t -> print_string (exportLTS_TrasitionsAux oc a t);;


let exportLTS (processedLTS:((int,stoch_transition list) Hashtbl.t)) = let oc = open_out "lts.dat" in
  (Hashtbl.iter (exportLTS_Trasitions oc) processedLTS; close_out oc);;

let foldExportIdMap oc (id:int) (mc:model_component) = 
  fprintf oc "%s %s\n" (string_of_int id) (printModelCompact mc);;

let exportIdMap (idToModelComponentTable:((int, model_component) Hashtbl.t)) = let oc = open_out "idMap.dat" in
  (Hashtbl.iter (foldExportIdMap oc) idToModelComponentTable; close_out oc);;


(*----------------------------------------------------*)
(*------ Operations on actions and action lists ------*)
(*----------------------------------------------------*)


let rec inSet s set =
  match set with
    [] -> false
  | h::t -> (h = s) || (inSet s t);;

let rec subsetOf s1 s2 =
  match s1 with
    [] -> true
   | h::t -> (inSet h s2) && (subsetOf t s2);;

let rec cardinality set =
  match set with
    [] -> 0
  | h::t -> 1 + cardinality t;;

let rec unionOf s1 s2 =
  match s2 with
    [] -> s1
  | h::t -> if (inSet h s1) then unionOf s1 t else (unionOf s1 t)@[h];;

let rec intersectionOf s1 s2 = 
  match s2 with
    [] -> []
  | h::t -> if inSet h s1 then h::(intersectionOf s1 t) else (intersectionOf s1 t);;

let rec setEqual s1 s2 = subsetOf s1 s2 && subsetOf s2 s1;;


module Activity_set = Set.Make (struct
                                  type t = action list
                                  let compare = compare
                                end);;


(*-----------------------------------------------*)
(*------- Abstract Machine main functions -------*)
(*-----------------------------------------------*)


let rec find_activitiesk (seq:(sequential_component)) (t:(action list)) (env:(assignment list)) (k:int) =
  match seq with
    Nil -> []
  | Choice(s1,s2) -> unionOf (find_activitiesk s1 t env k) (find_activitiesk s2 t env k)
  | Prefix(act,hook,const) -> if ((subsetOf act t) && (cardinality act = k)) then [RawTransition(act,hook,env,ConstModel(const))] else [];;

let rec find_activities_loop (seq:(sequential_component)) (t:(action list)) (env:(assignment list)) (card:int) =
  if card <=0 then [] else
    let result = find_activitiesk seq t env card in
    if result <> [] then result else find_activities_loop seq t env (card - 1);;

let find_activities (seq:(sequential_component)) (t:(action list)) (env:(assignment list)) =
  let card = cardinality t in
  find_activities_loop seq t env card;;

exception ConstantNotDefined;;

let rec species (const:constant) (seq_comp_def_list:sequential_component_definition list) =
  match seq_comp_def_list with
    [] -> raise ConstantNotDefined
  | SeqComp(constant,var_species,var_level,sequential_component)::t -> if constant = const then var_species else species const t;;

let rec level (const:constant) (seq_comp_def_list:sequential_component_definition list) =
  match seq_comp_def_list with
    [] -> raise ConstantNotDefined
  | SeqComp(constant,var_species,var_level,sequential_component)::t -> if constant = const then var_level else level const t;;

let rec seqCompOfConst (const:constant) (seq_comp_def_list:sequential_component_definition list) =
  match seq_comp_def_list with
    [] -> raise ConstantNotDefined
  | SeqComp(constant,var_species,var_level,sequential_component)::t -> if constant = const then sequential_component else seqCompOfConst const t;;

let rec searchSyncAux1 (res:(raw_transition list)) (l:(action list)) (p2:model_component) (acc:(raw_transition list)) =
  match res with
    [] -> acc
  | RawTransition(act,hook,env,model)::t -> if intersectionOf act l = [] then searchSyncAux1 t l p2 (unionOf [RawTransition(act,hook,env,Sync(l,model,p2))] acc) else searchSyncAux1 t l p2 acc;;

let rec searchSyncAux2 (res:(raw_transition list)) (l:(action list)) (p1:model_component) (acc:(raw_transition list)) =
  match res with
    [] -> acc
  | RawTransition(act,hook,env,model)::t -> if intersectionOf act l = [] then searchSyncAux2 t l p1 (unionOf [RawTransition(act,hook,env,Sync(l,p1,model))] acc) else searchSyncAux2 t l p1 acc;;

let rec searchSyncAux3 (res1:(raw_transition list)) (transition:raw_transition) (l:(action list)) (acc:(raw_transition list)) = 
  match res1 with
    [] -> acc
  | RawTransition(act1,hook1,env1,model1)::t -> 
      match transition with
        RawTransition(act2,hook2,env2,model2) -> let intAct12 = (intersectionOf act1 act2) in 
                                              if subsetOf intAct12 l && intAct12 <> [] then searchSyncAux3 t transition l (unionOf [RawTransition((unionOf act1 act2),(unionOf hook1 hook2),(unionOf env1 env2),Sync(l,model1,model2))] acc) else searchSyncAux3 t transition l acc;;

let rec searchSyncAux4 (res1:(raw_transition list)) (res2:(raw_transition list)) (l:(action list)) (acc:(raw_transition list)) = 
  match res2 with
    [] -> acc
  | h::t -> searchSyncAux4 res1 t l (unionOf (searchSyncAux3 res1 h l []) acc);;

let rec searchListenAux2 (res2:(raw_transition list)) (transition:raw_transition) (l:(action list)) (acc:(raw_transition list)) = 
  match res2 with
    [] -> acc
  | RawTransition(act2,hook2,env2,ConstModel(model2))::t -> 
      (match transition with
        RawTransition(act1,hook1,env1,model1) -> searchListenAux2 t transition l (unionOf [RawTransition((unionOf act1 act2),(unionOf hook1 hook2),(unionOf env1 env2),Listen(l,model1,model2))] acc))
  | _ -> acc;; (* This line never happens *)

let rec searchListenAux1 (stepsize:float) (res:(raw_transition list)) (l:(action list)) (constant:constant) (seq_comp_def_list:(sequential_component_definition list)) (acc:(raw_transition list)) =
  match res with
    [] -> acc
  | RawTransition(act,hook,env,model)::t -> let intersection = (intersectionOf hook l) in
                                         if intersection = [] then
                                            searchListenAux1 stepsize t l constant seq_comp_def_list (unionOf [RawTransition(act,hook,env,Listen(l,model,constant))] acc)
                                         else let res2 = (search stepsize (ConstModel(constant)) intersection seq_comp_def_list) in searchListenAux2 res2 (RawTransition(act,hook,env,model)) l (searchListenAux1 stepsize t l constant seq_comp_def_list acc)

and search (stepsize:float) (p:(model_component)) (t:(action list)) (seq_comp_def_list:(sequential_component_definition list)) =
  match p with
    Sync(actionlist,p1,p2) -> 
      let res1 = search stepsize p1 t seq_comp_def_list in
      let res2 = search stepsize p2 t seq_comp_def_list in
      let return1 = searchSyncAux1 res1 actionlist p2 [] in
      let return2 = searchSyncAux2 res2 actionlist p1 return1 in 
      searchSyncAux4 res1 res2 actionlist return2
  | Listen(actionlist,p1,constant) ->
      let res1 = search stepsize p1 t seq_comp_def_list in
      searchListenAux1 stepsize res1 actionlist constant seq_comp_def_list []
  | ConstModel(constant) -> if species constant seq_comp_def_list = "Bot" then 
                              let env = [] in find_activities (seqCompOfConst constant seq_comp_def_list) t env
                            else let env = [Assignment((species constant seq_comp_def_list),(float_of_int (level constant seq_comp_def_list)) *. stepsize)] in find_activities (seqCompOfConst constant seq_comp_def_list) t env;;

let rec multipleSearch_aux (stepsize:float) (p:(model_component)) (t_set:(action list)) (seq_comp_def_list:(sequential_component_definition list)) (acc:(raw_transition list)) = 
  match t_set with
    [] -> acc
  | h::t -> multipleSearch_aux stepsize p t seq_comp_def_list (unionOf (search stepsize p [h] seq_comp_def_list) acc);;

let multipleSearch (stepsize:float) (p:(model_component)) (t_set:(action list)) (seq_comp_def_list:(sequential_component_definition list)) = multipleSearch_aux stepsize p t_set seq_comp_def_list [];;

let rec computeLTS_aux2 (stepsize:float) (transitions:(raw_transition list)) (hashTable:((model_component,(raw_transition list)) Hashtbl.t)) =
  match transitions with
    [] -> []
  | RawTransition(act,hook,env,model)::t -> if Hashtbl.mem hashTable model then computeLTS_aux2 stepsize t hashTable else model::(computeLTS_aux2 stepsize t hashTable);;


let rec computeLTS_aux (stepsize:float) (toCheck:(model_component list)) (biochemicalActions:(action list)) (seq_comp_def_list:(sequential_component_definition list)) (hashTable:((model_component,(raw_transition list)) Hashtbl.t)) = 
  match toCheck with
    [] -> ()
  | h::t -> let transitions = multipleSearch stepsize h biochemicalActions seq_comp_def_list in
            (Hashtbl.add hashTable h transitions; (computeLTS_aux stepsize (unionOf t (computeLTS_aux2 stepsize transitions hashTable)) biochemicalActions seq_comp_def_list hashTable));;

let computeRawUnProcessedLTS (stepsize:float) (initialModelComp:model_component) (biochemicalActions:(action list)) (seq_comp_def_list:(sequential_component_definition list)) (hashTable:((model_component,(raw_transition list)) Hashtbl.t)) = computeLTS_aux stepsize [initialModelComp] biochemicalActions seq_comp_def_list hashTable;;

(* Substitute model components with integers IDs *)

let populateIdTablesAux (modelComponentToIdTable:((model_component, int) Hashtbl.t)) (idToModelComponentTable:((int, model_component) Hashtbl.t)) (c:model_component) (tr:(raw_transition list)) (n:int) = Hashtbl.add modelComponentToIdTable c n; Hashtbl.add idToModelComponentTable n c; n+1;;

let populateIdTables (modelComponentToIdTable:((model_component, int) Hashtbl.t)) (idToModelComponentTable:((int, model_component) Hashtbl.t)) (rawUnProcessedLTS:((model_component, raw_transition list) Hashtbl.t)) = Hashtbl.fold (populateIdTablesAux modelComponentToIdTable idToModelComponentTable) rawUnProcessedLTS 1;;

let rec substituteModelComponentsWithIDsAux (modelComponentToIdTable:((model_component, int) Hashtbl.t)) (transitions:(raw_transition list)) (aux:(transition list)) = 
  match transitions with
    [] -> aux
  | RawTransition(act,hook,env,model)::t -> substituteModelComponentsWithIDsAux modelComponentToIdTable t (Transition(act,hook,env,(Hashtbl.find modelComponentToIdTable model))::aux);;

let substituteModelComponentsWithIDs (modelComponentToIdTable:((model_component, int) Hashtbl.t)) (unProcessedLTS:((int, transition list) Hashtbl.t)) (rawUnProcessedLTS:((model_component, raw_transition list) Hashtbl.t)) (c:model_component) (tr:(raw_transition list)) = Hashtbl.add unProcessedLTS (Hashtbl.find modelComponentToIdTable c) (substituteModelComponentsWithIDsAux modelComponentToIdTable tr []); Hashtbl.remove rawUnProcessedLTS c;; 


let computeUnProcessedLTS (modelComponentToIdTable:((model_component, int) Hashtbl.t)) (rawUnProcessedLTS:((model_component, raw_transition list) Hashtbl.t)) (unProcessedLTS:((int, transition list) Hashtbl.t))  = Hashtbl.iter (substituteModelComponentsWithIDs modelComponentToIdTable unProcessedLTS rawUnProcessedLTS) rawUnProcessedLTS;;


(*--------------------------------------*)
(*--------- Functional Rates -----------*)
(*--------------------------------------*)

exception VarNotDefined;;

let rec searchEnvironment (env:(assignment list)) (varname:(string)) =
  match env with
    [] -> raise VarNotDefined
  | Assignment(name,value)::t -> if name = varname then value else searchEnvironment t varname;;

let rec eval (env:(assignment list)) (funrate:(fun_rate)) =
  match funrate with
    Real(value) -> value 
  | VarName(name) -> searchEnvironment env name
  | Sum(funrate1,funrate2) -> (eval env funrate1) +. (eval env funrate2)
  | Diff(funrate1,funrate2) -> (eval env funrate1) -. (eval env funrate2)
  | Prod(funrate1,funrate2) -> (eval env funrate1) *. (eval env funrate2)
  | Div(funrate1,funrate2) -> (eval env funrate2) /. (eval env funrate2)
  | Power(funrate1,funrate2) -> (eval env funrate1)**(eval env funrate2)
  | Exp(funrate1) -> exp (eval env funrate1)
  | Log(funrate1) -> log (eval env funrate1)
  | Sin(funrate1) -> sin (eval env funrate1)
  | Cos(funrate1) -> cos (eval env funrate1);;

exception FunRateNotDefined;;

let rec searchFunRate (funRateDef:(fun_rate_def list)) (action:action) =
  match funRateDef with
    [] -> raise FunRateNotDefined
  | FunRateDef(act,rate)::t -> if act = action then rate else searchFunRate t action;;

exception NoBioActFound;;

let rec searchBioAct (biochemicalActions:(action list)) (activity:(action list)) =
  match activity with
    [] -> raise NoBioActFound
  | h::t -> if inSet h biochemicalActions then h else searchBioAct biochemicalActions t;;

(*--------------------------------------*)
(*--------- Label Processing -----------*)
(*--------------------------------------*)


let rec stochastic_processingAux (modelenv:(assignment list)) (selectedActions:(action list)) (biochemicalActions:(action list)) (funRateDef:(fun_rate_def list)) (a:(int)) (transitions:(transition list)) (aux:(stoch_transition list)) = 
  match transitions with
    [] -> aux
  | Transition(act,hook,env,id)::t -> let funrate = (searchFunRate funRateDef (searchBioAct biochemicalActions act)) in 
let intOfActAndSelAct = (intersectionOf act selectedActions) in
stochastic_processingAux modelenv selectedActions biochemicalActions funRateDef a t (StochTransition((if intOfActAndSelAct = [] then [] else intOfActAndSelAct),(eval (unionOf env modelenv) funrate),id)::aux);;

let stochastic_processingFun (env:(assignment list)) (selectedActions:(action list)) (biochemicalActions:(action list)) (funRateDef:(fun_rate_def list)) (hashTable:((int,(transition list)) Hashtbl.t)) (hashTable2:((int,(stoch_transition list)) Hashtbl.t)) (a:int) (transitions:(transition list)) = Hashtbl.add hashTable2 a (stochastic_processingAux env selectedActions biochemicalActions funRateDef a transitions []); Hashtbl.remove hashTable a;;

(*  *)

let stochasticProcessing (env:(assignment list)) (selectedActions:(action list)) (biochemicalActions:(action list)) (funRateDef:(fun_rate_def list)) (unProcessedLTS:((int,(transition list)) Hashtbl.t)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) = Hashtbl.iter (stochastic_processingFun env selectedActions biochemicalActions funRateDef unProcessedLTS processedLTS) unProcessedLTS;;


(*--------------------------------------*)
(*---------- Bisimulation --------------*)
(*--------------------------------------*)

(*TODO use sets also for activities! or at least make it that x,y = y,x*)

let rec getActivitiesAux (transitions:(stoch_transition list)) (acc:(action list list)) = 
  match transitions with
    [] -> acc
  | StochTransition(act,rate,model)::t ->  getActivitiesAux t (unionOf [act] acc);;

let getActivitiesAux1 (p1:(int)) (transitions:(stoch_transition list)) (acc:(action list list)) = unionOf (getActivitiesAux transitions []) acc;;

let getActivitiesProcessedLTS (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) = Hashtbl.fold getActivitiesAux1 processedLTS [];;

let getStatesProcessedLTSaux (p1:(int)) (transitions:(stoch_transition list)) (acc:(int list)) = p1::acc;;

let getStatesProcessedLTS (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) = Hashtbl.fold getStatesProcessedLTSaux processedLTS [];;


let rec getRateAux (act:(action list)) (p2:(int)) (transitions:(stoch_transition list)) = 
  match transitions with
    [] -> 0.
  | StochTransition(act',rate,model)::t -> if act' = act && model = p2 then rate else getRateAux act p2 t;; 

let getRate (p1:int) (act:(action list)) (p2:(int)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) = getRateAux act p2 (Hashtbl.find processedLTS p1);;


let rec apparentRateToClass (p1:int) (act:(action list)) (pClass:(int list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) (acc:float) = 
  match pClass with
    [] -> acc
  | h::t -> apparentRateToClass p1 act t processedLTS ((getRate p1 act h processedLTS) +. acc);;

let bisimulationCondition (p1:int) (p2:int) (act:(action list)) (pClass:(int list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) = (apparentRateToClass p1 act pClass processedLTS 0.) = (apparentRateToClass p2 act pClass processedLTS 0.);;

let rec forAllClasses (p1:int) (p2:int) (act:(action list)) (classesList:(int list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) (acc:bool) =
  match classesList with
    [] -> acc
  | h::t -> forAllClasses p1 p2 act t processedLTS ((bisimulationCondition p1 p2 act h processedLTS) && acc);;

let rec forAllActivitiesAndClasses (p1:int) (p2:int) (actList:(action list list)) (classesList:(int list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) (acc:bool) =  
  match actList with
    [] -> acc
  | h::t -> forAllActivitiesAndClasses p1 p2 t classesList processedLTS ((forAllClasses p1 p2 h classesList processedLTS true) && acc);;

let rec combinations (p:(int)) (pList:(int list)) (acc:((int * int) list)) =
  match pList with
    [] -> acc
  | h::t -> combinations p t ((p,h)::acc);;

let rec initialRelationAux (statesList:(int list)) (acc:((int * int) list)) =
  match statesList with
    [] -> acc
  | h::t -> initialRelationAux t ((combinations h t [])@acc);;

let getInitialRelation (statesList:(int list)) = initialRelationAux statesList [];;


let rec combinationsSmart (actList:(action list list)) (classesList:(int list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) (p:int) (pList:(int list)) (acc:((int * int) list)) =
  match pList with
    [] -> acc
  | h::t -> if (forAllActivitiesAndClasses p h actList classesList processedLTS true) then combinationsSmart actList classesList processedLTS p t ((p,h)::acc) else combinationsSmart actList classesList processedLTS p t acc;;

let rec initialRelationAuxSmart (actList:(action list list)) (classesList:(int list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) (statesList:(int list)) (acc:((int * int) list)) =
  match statesList with
    [] -> acc
  | h::t -> initialRelationAuxSmart actList classesList processedLTS t ((combinationsSmart actList classesList processedLTS h t [])@acc);;

let getInitialRelationSmart (actList:(action list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) (statesList:(int list))  = initialRelationAuxSmart actList [statesList] processedLTS statesList [];;

let rec insertEquivalence (p1:int) (p2:int) (equivalenceClasses:(int list list)) = 
  match equivalenceClasses with
    [] -> [[p1;p2]]
  | h::t -> if (inSet p1 h) then (unionOf [p2] h)::t 
            else if (inSet p2 h) then (p1::h)::t
            else h::(insertEquivalence p1 p2 t);;

let rec buildEquivalenceClassesAux (relation:((int * int) list)) (acc:(int list list)) =
  match relation with
    [] -> acc
  | (p1,p2)::t -> buildEquivalenceClassesAux t (insertEquivalence p1 p2 acc);;

let rec insertTrivialEquivalencesAux (p:int) (equivalenceClasses:(int list list)) =
  match equivalenceClasses with
    [] -> [[p]]
  | h::t -> if (inSet p h) then equivalenceClasses else h::(insertTrivialEquivalencesAux p t);;

let rec insertTrivialEquivalences (statesList:(int list)) (equivalenceClasses:(int list list)) =
  match statesList with
    [] -> equivalenceClasses
  | h::t -> insertTrivialEquivalences t (insertTrivialEquivalencesAux h equivalenceClasses);;

let buildEquivalenceClasses (relation:((int * int) list)) (statesList:(int list)) = insertTrivialEquivalences statesList (buildEquivalenceClassesAux relation []);;

let rec functionalOfBisimulationAux (relation:((int * int) list)) (actList:(action list list)) (eClasses:(int list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) (acc:((int * int) list)) =
    match relation with
      [] -> acc
    | (a,b)::t -> if (forAllActivitiesAndClasses a b actList eClasses processedLTS true) then functionalOfBisimulationAux t actList eClasses processedLTS ((a,b)::acc) else functionalOfBisimulationAux t actList eClasses processedLTS acc;;

let functionalOfBisimulation (relation:((int * int) list)) (statesList:(int list)) (actList:(action list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) = 
  let eClasses = buildEquivalenceClasses relation statesList in
    (eClasses, functionalOfBisimulationAux relation actList eClasses processedLTS []);;

let rec bisimulationMinimisationAux (relation:((int * int) list)) (statesList:(int list)) (actList:(action list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) =
  let (eClasses, newRelation) = functionalOfBisimulation relation statesList actList processedLTS in
  if subsetOf relation newRelation then eClasses else bisimulationMinimisationAux newRelation statesList actList processedLTS;;

let bisimulationMinimisation (activitiesProcessedLTS:(action list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) =
  let statesProcessedLTS = getStatesProcessedLTS processedLTS in
  let initialRelation = getInitialRelationSmart activitiesProcessedLTS processedLTS statesProcessedLTS in 
(*  let initialRelation = getInitialRelation statesProcessedLTS in *)
  bisimulationMinimisationAux initialRelation statesProcessedLTS activitiesProcessedLTS processedLTS;;


let rec buildMinimisedLTSaux2 (act:(action list)) (p:int) (plist:(int list)) (equivalenceClasses:(int list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) (acc:(minimised_transition list)) =
  match equivalenceClasses with
    [] -> acc
  | h::t -> let rate = (apparentRateToClass p act h processedLTS 0.) in
            if (rate = 0. || setEqual plist h) then buildMinimisedLTSaux2 act p plist t processedLTS acc 
            else buildMinimisedLTSaux2 act p plist t processedLTS ((MinimisedTransition(act,rate,h))::acc);;

(* debug function
let rec buildMinimisedLTSaux2 (act:(action list)) (p:int) (plist:(int list)) (equivalenceClasses:(int list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) (acc:(minimised_transition list)) =
  match equivalenceClasses with
    [] -> acc
  | h::t -> let rate = (apparentRateToClass p act h processedLTS 0.) in
            print_string ("Classes to go\n"^(printEquivalenceClassesIDs equivalenceClasses)^"\n");
            print_string ("from equivalence class "^(printEquivalenceClassIDs plist)^"\n"); 
            print_string ("to equivalence class "^(printEquivalenceClassIDs h)^"\n");
            print_string ("with activity "^(printActivity act)^"\n");
            print_string ("rate is "^(string_of_float rate)^"\n");
            if (rate = 0. || setEqual plist h) then buildMinimisedLTSaux2 act p plist t processedLTS acc 
            else buildMinimisedLTSaux2 act p plist t processedLTS ((MinimisedTransition(act,rate,h))::acc);;
*)

let rec buildMinimisedLTSaux (activitiesProcessedLTS:(action list list)) (p:int) (plist:(int list)) (equivalenceClasses:(int list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) (acc:(minimised_transition list)) =
  match activitiesProcessedLTS with
    [] -> acc
  | h::t -> buildMinimisedLTSaux t p plist equivalenceClasses processedLTS ((buildMinimisedLTSaux2 h p plist equivalenceClasses processedLTS [])@acc);;

let rec buildMinimisedLTSb (activitiesProcessedLTS:(action list list)) (equivalenceClassesToPassIntact:(int list list)) (equivalenceClasses:(int list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) (minimisedLTS:((int list,(minimised_transition list)) Hashtbl.t)) = 
  match equivalenceClasses with
    [] -> ()
  | h::t -> match h with 
              [] -> () (* impossible *)
            | p::xp -> Hashtbl.add minimisedLTS h (buildMinimisedLTSaux activitiesProcessedLTS p h equivalenceClassesToPassIntact processedLTS []); buildMinimisedLTSb activitiesProcessedLTS equivalenceClassesToPassIntact t processedLTS minimisedLTS;;

let buildMinimisedLTS(activitiesProcessedLTS:(action list list)) (equivalenceClasses:(int list list)) (processedLTS:((int,(stoch_transition list)) Hashtbl.t)) (minimisedLTS:((int list,(minimised_transition list)) Hashtbl.t)) = buildMinimisedLTSb activitiesProcessedLTS equivalenceClasses equivalenceClasses processedLTS minimisedLTS;;

