#! /bin/bash

ocamlc -c multiset.mli
ocamlc -c multiset.ml
ocamlc -c AM.mli
ocamlc -c AM.ml
ocamllex lexer.mll
ocamlyacc parser.mly

ocamlc -c parser.mli
ocamlc -c parser.ml
ocamlc -c lexer.ml
ocamlc -o interpret multiset.cmo AM.cmo parser.cmo lexer.cmo program.ml
cp interpret ..
cp interpret ../tests
